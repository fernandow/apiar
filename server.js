//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/eballesterosf/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/clientes/:idcliente", function(req, res){
    res.send('el numero de cliente es ' + req.params.idcliente); // 'Hola mundo'
});

app.get("/v1/movimientos", function(req, res){
    res.sendFile(path.join(__dirname, 'movimientosv1.json')); // 'Hola mundo'
});

app.get("/v1/movimientos", function(req, res){
    res.sendFile(path.join(__dirname, 'movimientosv1.json')); // 'Hola mundo'
});

var movimientosJSONV2 = require("./movimientosv2.json");

app.get("/v2/movimientos", function(req, res){
    res.send(movimientosJSONV2); // 'Hola mundo'
});

app.get("/v2/movimientosq", function(req, res){
  console.log(req.query);
  res.send();
});

app.get("/v2/movimientos/:id", function(req, res){
    res.send(movimientosJSONV2[req.params.id]); // 'Hola mundo'
});

app.get("/sarasa", function(req, res){
    res.sendFile(path.join(__dirname, 'index.html')); // 'Hola mundo'
});

app.post("/", function(req,res){
    res.send('Hemos recibido su peticion cambiada');
});

app.post("/v2/movimientos", function(req,res){
    var nuevo = req.body;
    nuevo.id = movimientosJSONV2.length + 1;
    movimientosJSONV2.push(nuevo);


    res.send('Movimiento dado de alta');
});
